variable "oci_tenancy" {
  type = string
}

variable "oci_compartment" {
  type = string
}

variable "oci_compartment_name" {
  type    = string
  default = "openworld"
}

variable "oci_namespace" {
  type = string
}

variable "oci_compute_shape" {
  type    = string
  default = "VM.Standard.E2.1.Micro"
}

variable "oci_compute_shape_flex" {
  type    = bool
  default = false
}

variable "oci_compute_memory" {
  type    = number
  default = 6
}

variable "oci_compute_ocpus" {
  type    = number
  default = 3
}

variable "oci_volume_size" {
  type    = number
  default = 50
}

variable "oci_compute_display_name" {
  type    = string
  default = "openworld"
}

variable "openworld_port" {
  type    = number
  default = 25555
}

variable "oci_image_id" {
  type    = string
  default = "ocid1.image.oc1.uk-london-1.aaaaaaaazgtvwb26enkylhdxlnsoit2le76cfilvpmpgny6migdeth74iwcq"
}

variable "ssh_authorized_keys" {
  type = string
}

variable "static_ip" {
  type    = bool
  default = true
}

variable "management_ip" {
  type = string
}

variable "backup_bucket_name" {
  type    = string
  default = "openworld-backup"
}
