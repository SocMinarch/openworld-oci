resource "oci_core_network_security_group" "openworld" {
  compartment_id = oci_identity_compartment.openworld_compartment.id
  vcn_id         = oci_core_vcn.vcn.id

  display_name = var.oci_compute_display_name
}

resource "oci_core_network_security_group_security_rule" "ssh" {
  network_security_group_id = oci_core_network_security_group.openworld.id
  direction                 = "INGRESS"
  protocol                  = 6
  source                    = var.management_ip
  source_type               = "CIDR_BLOCK"

  tcp_options {
    destination_port_range {
      min = 22
      max = 22
    }
  }
}

resource "oci_core_network_security_group_security_rule" "openworld" {
  network_security_group_id = oci_core_network_security_group.openworld.id
  direction                 = "INGRESS"
  protocol                  = 6
  source                    = "0.0.0.0/0"
  source_type               = "CIDR_BLOCK"

  tcp_options {
    destination_port_range {
      min = var.openworld_port
      max = var.openworld_port
    }
  }
}

resource "oci_core_network_security_group_security_rule" "egress" {
  network_security_group_id = oci_core_network_security_group.openworld.id
  direction                 = "EGRESS"
  protocol                  = "all"
  destination               = "0.0.0.0/0"
  source_type               = "CIDR_BLOCK"
}
