resource "oci_identity_compartment" "openworld_compartment" {
  compartment_id = var.oci_compartment
  description    = "openworld"
  name           = var.oci_compartment_name
}
