resource "oci_identity_dynamic_group" "openworld" {
  compartment_id = var.oci_tenancy
  description    = "openworld"
  name           = var.oci_compartment_name
  matching_rule  = "ANY {instance.compartment.id = '${oci_identity_compartment.openworld_compartment.id}'}"
}

resource "oci_identity_policy" "openworld_backup" {
  compartment_id = oci_identity_compartment.openworld_compartment.id
  description    = "openworld backup"
  name           = var.backup_bucket_name
  statements = [
    "Allow dynamic-group ${oci_identity_dynamic_group.openworld.name} to read buckets in compartment id ${oci_identity_compartment.openworld_compartment.id} where target.bucket.name='${oci_objectstorage_bucket.openworld_backup.name}'",
    "Allow dynamic-group ${oci_identity_dynamic_group.openworld.name} to manage objects in compartment id ${oci_identity_compartment.openworld_compartment.id} where all {target.bucket.name='${oci_objectstorage_bucket.openworld_backup.name}', any {request.permission='OBJECT_INSPECT', request.permission='OBJECT_READ', request.permission='OBJECT_CREATE', request.permission='OBJECT_OVERWRITE'}}"
  ]
}
