# yaml-language-server: $schema=https://json.schemastore.org/ansible-playbook.json

- hosts: openworld

  vars:
    memtotal_b: "{{ ansible_memtotal_mb * 1048576 }}"

  tasks:
    - name: Configuring sshd # Based on https://infosec.mozilla.org/guidelines/openssh and CIS Benchmarks
      lineinfile:
        path: /etc/ssh/sshd_config
        regexp: "^({{ item.regexp }}|#{{ item.regexp }})"
        line: "{{ item.line }}"
      loop:
        - {
            regexp: "KexAlgorithms",
            line: "KexAlgorithms curve25519-sha256@libssh.org",
          }
        - { regexp: "Ciphers", line: "Ciphers chacha20-poly1305@openssh.com" }
        - {
            regexp: "MACs",
            line: "MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com",
          }
        - {
            regexp: "AuthenticationMethods",
            line: "AuthenticationMethods publickey",
          }
        - { regexp: "LogLevel", line: "LogLevel VERBOSE" }
        - {
            regexp: "Subsystem sftp",
            line: "Subsystem sftp /usr/libexec/openssh/sftp-server -f AUTHPRIV -l INFO",
          }
        - { regexp: "PermitRootLogin", line: "PermitRootLogin no" }
        - {
            regexp: "UsePrivilegeSeparation",
            line: "UsePrivilegeSeparation sandbox",
          }
        - { regexp: "LoginGraceTime", line: "LoginGraceTime 1m" }
        - { regexp: "AllowTcpForwarding", line: "AllowTcpForwarding no" }
        - { regexp: "MaxSessions", line: "MaxSessions 4" }
        - { regexp: "X11Forwarding", line: "X11Forwarding no" }
        - { regexp: "MaxAuthTries", line: "MaxAuthTries 4" }
      register: sshd

    - name: Checking if SSH short moduli needs disabling
      command: awk '{ if ($5 < 3071) { exit 3 } else { exit 0 } }' /etc/ssh/moduli
      changed_when: moduli.rc == 3
      failed_when:
        - moduli.rc != 0
        - moduli.rc != 3
      register: moduli

    - name: Disabling SSH short moduli
      command: awk -i inplace '$5 >= 3071' /etc/ssh/moduli
      when: moduli.changed

    - name: Restarting sshd
      systemd:
        name: sshd
        state: restarted
      when: sshd.changed or moduli.changed

    - name: Populate service facts
      ansible.builtin.service_facts:

    - name: Stopping the existing server
      systemd:
        name: openworld
        state: stopped
      when: ansible_facts.services['openworld.service'] is defined

    - name: Backing up the existing server
      shell: /opt/openworld/backup.sh
      args:
        chdir: /opt/openworld
      when:
        - state.new_server or state.update_server
        - state.skip_backup == false

    - name: Applying updates with Uptrack
      command: uptrack-upgrade -y

    - name: Enabling EPEL
      yum_repository:
        name: ol8_developer_EPEL
        baseurl: https://yum$ociregion.$ocidomain/repo/OracleLinux/OL8/developer/EPEL/$basearch/
        description: Oracle Linux $releasever EPEL Packages for Development ($basearch)
        gpgcheck: yes
        gpgkey: file:///etc/pki/rpm-gpg/RPM-GPG-KEY-oracle
        enabled: yes
        file: oracle-epel-ol8

    - name: Installing updates
      dnf:
        name: "*"
        state: latest
        update_cache: yes

    - name: Setting the timezone
      timezone:
        name: "{{ timezone }}"

    - name: Installing packages
      dnf:
        name:
          - jq
          - borgbackup
          - libicu
          - python36-oci-cli
          - tmux
        state: present
        update_cache: yes

    - name: Installing .NET
      dnf:
        name: dotnet-runtime-6.0
        state: present
        update_cache: yes
      when: ansible_architecture == 'x86_64'

    - name: Getting the latest Grafana Agent release
      shell: curl --silent https://api.github.com/repos/grafana/agent/releases/latest | jq -r '.assets[].browser_download_url | select(contains("{% if ansible_architecture == 'aarch64' %}arm64{% elif ansible_architecture == 'x86_64' %}amd64{% endif %}")) | select(contains("rpm"))'
      register: grafanaagent_latest
      when:
        - grafana.use

    - name: Installing Grafana Agent
      dnf:
        name: "{{ grafanaagent_latest.stdout }}"
        state: present
        disable_gpg_check: yes
      when: grafana.use

    - name: Creating the openworld user
      user:
        name: openworld
        create_home: yes
        home: /opt/openworld
        shell: /bin/bash

    - name: Checking the size of the swapfile
      stat:
        path: /.swapfile
      register: swapfile

    - name: Turning off swapfile
      command: swapoff /.swapfile
      changed_when: swapoff.rc == 0
      failed_when:
        - swapoff.rc != 0
        - swapoff.rc != 255
      when: swapfile.stat.size != memtotal_b
      register: swapoff

    - name: Removing the existing swapfile
      file:
        path: /.swapfile
        state: absent
      when: swapfile.stat.size != memtotal_b

    - name: Creating swapfile to match system memory
      command: fallocate -l {{ memtotal_b }} /.swapfile
      when: swapfile.stat.size != memtotal_b

    - name: Setting permissions for the swapfile
      file:
        path: /.swapfile
        owner: root
        group: root
        mode: 0600

    - name: Formatting swapfile
      command: mkswap /.swapfile
      when: swapfile.stat.size != memtotal_b

    - name: Changing vm.swappiness to 1 # Oracle Linux recommendation for SSD's
      sysctl:
        name: vm.swappiness
        value: 1
        reload: yes

    - name: Turning on swapfile
      command: swapon /.swapfile
      when: swapfile.stat.size != memtotal_b

    - name: Removing the existing server
      file:
        path: /opt/openworld/server
        state: absent
      when: state.new_server or state.backup.restore_backup

    - name: Creating the Open World directory
      file:
        path: /opt/openworld/server
        state: directory
        owner: openworld
        group: openworld
        mode: u=rwX,g=rX,o=rX
        recurse: yes

    - name: Removing the existing backup repository
      file:
        path: /opt/openworld/backup
        state: absent
      loop: "{{ instances }}"
      when: state.backup.new_repository

    - name: Creating the backup directory
      file:
        path: /opt/openworld/backup
        state: directory
        owner: openworld
        group: openworld
        mode: 0755
        recurse: yes
      when:
        - state.backup.restore_backup == false

    - name: Initialising the instance backup repository
      command: borg init -e none /opt/openworld/backup
      changed_when: borg.rc == 0
      failed_when:
        - borg.rc != 0
        - borg.rc != 2
      register: borg
      when:
        - state.backup.restore_backup == false
      become_user: openworld

    - name: Opening the backup repository once
      shell: BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes borg list /opt/minecraft/{{ item.name }}/backup
      become_user: openworld
      when: state.backup.new_repository

    - name: Opening the backup repository once as root
      shell: BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes borg list /opt/minecraft/{{ item.name }}/backup
      loop: "{{ instances }}"
      when: state.backup.new_repository

    - name: Setting the instance backup retention policy
      copy:
        content: "{{ state.backup.backup_retention_days }}"
        dest: /opt/openworld/backup_retention_days
        owner: openworld
        group: openworld
        mode: 0644

    - name: Downloading Open World
      unarchive:
        src: "https://github.com/TastyLollipop/OpenWorld/releases/download/1.3.6/{% if ansible_architecture == 'aarch64' %}ARMX64_SelfContained.zip{% elif ansible_architecture == 'x86_64' %}LinuxX64.zip{% endif %}"
        dest: /opt/openworld/server/
        owner: openworld
        group: openworld
        mode: u=rwX,g=rX,o=rX
        remote_src: yes
      become_user: openworld

    - name: Setting permissions for the Open World server
      file:
        path: /opt/openworld/server/Open World Server
        owner: openworld
        group: openworld
        mode: 0755

    - name: Configuring the Open World server
      copy:
        src: files/openworld/{{ item }}
        dest: /opt/openworld/server/{{ item }}
        owner: openworld
        group: openworld
        mode: 0644
      loop:
        - Server Settings.txt
        - Whitelisted Players.txt
        - World Settings.txt

    - name: Creating the Open World mods directory
      file:
        path: /opt/openworld/server/{{ item }}
        owner: openworld
        group: openworld
        mode: 0755
        state: directory
      loop:
        - Mods/OpenWorld
        - Whitelisted Mods

    - name: Enabling RimWorld DLC
      unarchive:
        src: https://github.com/TastyLollipop/OpenWorld/raw/1.3.6/Core%20%26%20DLCs.zip
        dest: /opt/openworld/server/Mods/
        group: openworld
        mode: u=rwX,g=rX,o=rX
        remote_src: yes
      become_user: openworld
      when: dlc

    - name: Getting the latest Harmony release
      shell: curl --silent "https://api.github.com/repos/pardeike/HarmonyRimWorld/releases/latest" | jq -r '.assets[].browser_download_url'
      register: harmony_latest

    - name: Getting the latest HugsLib release
      shell: curl --silent "https://api.github.com/repos/UnlimitedHugs/RimworldHugsLib/releases/latest" | jq -r '.assets[].browser_download_url'
      register: hugslib_latest

    - name: Adding the Harmony and HugsLib mods
      unarchive:
        src: "{{ item }}"
        dest: /opt/openworld/server/Mods/
        owner: openworld
        group: openworld
        mode: u=rwX,g=rX,o=rX
        remote_src: yes
      become_user: openworld
      loop:
        - "{{ harmony_latest.stdout }}"
        - "{{ hugslib_latest.stdout }}"

    - name: Copying the Open World mod
      copy:
        src: "{{ openworld_mod }}"
        dest: /opt/openworld/server/Mods/OpenWorld/
        owner: openworld
        group: openworld
        mode: u=rwX,g=rX,o=rX
      become_user: openworld

    - name: Downloading mods
      unarchive:
        src: "{{ item.url }}"
        dest: /opt/openworld/server/{% if item.required %}Mods{% else %}Whitelisted Mods{% endif %}/
        owner: openworld
        group: openworld
        mode: u=rwX,g=rX,o=rX
        remote_src: yes
      become_user: openworld
      loop: "{{ mods }}"
      when: mods is defined

    - name: Downloading the server backup
      shell: oci os object get --auth instance_principal --bucket-name {{ bucket_name }} --file - --name openworld.tar | tar xf -
      args:
        chdir: /opt/openworld
      when:
        - state.backup.restore_backup
        - state.backup.restore_remote_backup
      become_user: openworld

    - name: Restoring the server backup
      shell: "{% if state.backup.restore_remote_backup %}BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes {% endif %}borg extract /opt/openworld/backup::{{ state.backup.backup_version }}"
      args:
        chdir: /opt/openworld/server
      when:
        - state.backup.restore_backup
      become_user: openworld

    - name: Copying scripts and systemd units
      template:
        src: "{{ item.src }}"
        dest: "{{ item.dest }}"
        owner: "{{ item.owner }}"
        group: "{{ item.owner }}"
        mode: "{{ item.mode }}"
        validate: "{{ item.validate | default(omit) }}"
      loop:
        - {
            src: "files/scripts/dnf-reboot.sh",
            dest: "/opt/openworld/dnf-reboot.sh",
            owner: "root",
            mode: "0755",
            when: true,
          }
        - {
            src: "files/scripts/stop.sh",
            dest: "/opt/openworld/stop.sh",
            owner: "root",
            mode: "0755",
            when: true,
          }
        - {
            src: "templates/scripts/backup.sh.j2",
            dest: "/opt/openworld/backup.sh",
            owner: "root",
            mode: "0755",
            when: true,
          }
        - {
            src: "templates/systemd/service.j2",
            dest: "/etc/systemd/system/openworld.service",
            owner: "root",
            mode: "0644",
            description: "Open World server",
            type: "forking",
            execstart: "/usr/bin/tmux new-session -s openworld -d '{{ command }}'",
            execstop: "/opt/openworld/stop.sh",
            workingdirectory: "/opt/openworld/server",
            nice: "-20",
            oomscoreadjust: "-1000",
            user: "openworld",
            group: "openworld",
            when: true,
          }
        - {
            src: "templates/systemd/service.j2",
            dest: "/etc/systemd/system/openworld-backup.service",
            owner: "root",
            mode: "0644",
            description: "Open World server backup",
            type: "oneshot",
            execstart: "/opt/openworld/backup.sh restart",
            workingdirectory: "/opt/openworld",
            user: "openworld",
            when: true,
          }
        - {
            src: "templates/systemd/timer.j2",
            dest: "/etc/systemd/system/openworld-backup.timer",
            owner: "root",
            mode: "0644",
            description: "openworld Server backup",
            timer: "6:00",
            persistent: true,
            when: true,
          }
        - {
            src: "templates/systemd/service.j2",
            dest: "/etc/systemd/system/dnf-upgrade.service",
            owner: "root",
            mode: "0644",
            description: "DNF Upgrade",
            type: "oneshot",
            execstart: "/opt/openworld/dnf-reboot.sh",
            when: true,
          }
        - {
            src: "templates/systemd/timer.j2",
            dest: "/etc/systemd/system/dnf-upgrade.timer",
            owner: "root",
            mode: "0644",
            description: "DNF Upgrade",
            timer: "Tue 7:00",
            persistent: true,
            when: true,
          }
        - {
            src: "templates/grafana-agent.yml.j2",
            dest: "/etc/grafana-agent.yaml",
            owner: root,
            group: grafana-agent,
            mode: "0640",
            when: "{{ grafana.use }}",
          }
        - {
            src: "templates/systemd/service.j2",
            dest: "/etc/systemd/system/grafana-agent.service",
            owner: "root",
            mode: "0644",
            description: "Monitoring system and forwarder",
            execstart: "/usr/bin/grafana-agent --config.file /etc/grafana-agent.yaml",
            type: "simple",
            after: "network-online.target",
            when: "{{ grafana.use }}",
          }
        - {
            src: "files/sudoers",
            dest: "/etc/sudoers.d/10-openworld-user",
            owner: "root",
            mode: "0400",
            when: true,
            validate: "visudo -cf %s",
          }
      when: item.when

    - name: Creating directory for the SELinux module
      file:
        path: /opt/openworld/selinux
        state: directory
        owner: root
        group: root
        mode: 0755

    - name: Copying SELinux module
      template:
        src: templates/selinux.te.j2
        dest: /opt/openworld/selinux/openworld.te
        owner: root
        group: root
        mode: 0644
      register: selinux

    - name: Compiling SELinux module
      command: checkmodule -M -m -o openworld.mod openworld.te
      args:
        chdir: /opt/openworld/selinux
      when: selinux.changed

    - name: Packaging SELinux module
      command: semodule_package -o openworld.pp -m openworld.mod
      args:
        chdir: /opt/openworld/selinux
      when: selinux.changed

    - name: Loading SELinux module
      command: semodule -i openworld.pp
      args:
        chdir: /opt/openworld/selinux
      when: selinux.changed

    - name: Copying firewall rule
      template:
        src: templates/openworld.xml.j2
        dest: /etc/firewalld/services/openworld.xml
        owner: root
        group: root
        mode: 0644
      register: firewall

    - name: Reloading firewalld
      command: firewall-cmd --reload
      when: firewall.changed

    - name: Allowing Open World through the firewall
      firewalld:
        service: openworld
        state: enabled
        permanent: yes
        immediate: yes

    - name: Starting the systemd units
      systemd:
        name: "{{ item.name }}"
        state: "{{ item.state }}"
        enabled: "{{ item.enabled }}"
        daemon_reload: yes
      loop:
        - {
            name: "dnf-upgrade.timer",
            state: "restarted",
            enabled: "yes",
            when: "{{ true }}",
          }
        - {
            name: "grafana-agent.service",
            state: "restarted",
            enabled: "true",
            when: "{{ grafana.use }}",
          }
        - {
            name: "openworld.service",
            state: "started",
            enabled: "true",
            when: "{{ true }}",
          }
        - {
            name: "openworld-backup.timer",
            state: "started",
            enabled: "true",
            when: "{{ true }}",
          }
      when: item.when
